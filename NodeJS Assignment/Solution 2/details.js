var response = $.getJSON("/config.json", function() {
    var userInfos = response.responseJSON;
    var userDetailsTable = document.getElementsByClassName('user_info_table')[0];
    let tableRow = document.createElement("tr");
    for(let field in userInfos["data"][0]){
        let tableHeading = document.createElement("th");
        let heading = document.createTextNode(field);
        tableHeading.appendChild(heading);
        tableRow.appendChild(tableHeading);
    }
    userDetailsTable.appendChild(tableRow);
    for(let userInfo of userInfos["data"]){
        let tableRow = document.createElement("tr");
        for(let field in userInfo){
            let tableData = document.createElement("td");
            let data = document.createTextNode(userInfo[field]);
            tableData.appendChild(data);
            tableRow.appendChild(tableData);
        }
        userDetailsTable.appendChild(tableRow);
    }
  })
    .done(function() {
      console.log( "second success" );
    })
    .fail(function() {
      console.log( "error" );
    })
    .always(function() {
      console.log( "complete" );
    });

