var http = require('http');
var fs = require("fs");

function parseToJSON(body){
    body = body.replace('+', ' ');
    jsonObject = {};
    fields = body.split("&");
    for(var field in fields){
        [key, value] = fields[field].split("=");
        jsonObject[key] = value;
    }
    return jsonObject;
}

function onRequest(request, response) {
    console.log(`${request.method} Requesting ${request.url}`);
    response.writeHead(200, { "Content-type" : "text/html"});
    if(request.url==='/'){
        fs.readFile("./index.html", (err, data)=>{
            if(err){
                console.log(err);
            }
            else{
                response.write(data.toString('utf8'));
                response.end();
            }
        });
    }
    else if(request.url==='/details'){
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        request.on('end', () => {
            var newUserInfo = parseToJSON(body);
            fs.readFile("./config.json", (err, data)=>{
                if(err){
                    console.log(err);
                }
                else{
                    var userInfos = JSON.parse(data.toString());
                    userInfos.data.push(newUserInfo);
                    fs.writeFile("./config.json",JSON.stringify(userInfos), 'utf8', (err) => {
                        if(err){
                            console.log(err);
                        }
                        else{
                            console.log("New User Information added to file successfully.");
                            fs.readFile("./details.html", (err, data)=>{
                                if(err){
                                    console.log(err);
                                }
                                else{
                                    response.write(data.toString('utf8'));
                                    response.end();
                                }
                            });
                        }
                    });
                }
            });
        });
    }
    else if(request.url === '/details.js') {
        response.writeHead(200, { 'Content-type' : 'application/javascript'});
        fs.readFile('./details.js', (err, data)=>{
            if(err){
                console.log(err);
            }
            else{
                response.write(data);
                response.end();
            }
        });
    }
    else if(request.url === '/config.json') {
        response.writeHead(200, { 'Content-type' : 'application/json'});
        fs.readFile('./config.json', (err, data)=>{
            if(err){
                console.log(err);
            }
            else{
                response.write(data);
                response.end();
            }
        });
    }
}

http.createServer(onRequest).listen(3000);
console.log("Server running ....");