const game = require("rockpaperscissor-noufal");
const inquirer = require('inquirer')

var questions = [{
    type: 'input',
    name: 'player1Move',
    message: "Player 1, Enter your Move : ",
}, {
    type: 'input',
    name: 'player2Move',
    message: "Player 2, Enter your Move : ",
}, {
    type: 'input',
    name: 'player3Move',
    message: "Player 3, Enter your Move : ",
}]

function playRockPaperScissorGame(){
    inquirer.prompt(questions).then(answers => {
    let player1Move = answers['player1Move'];
    let player2Move = answers['player2Move'];
    let player3Move = answers['player3Move'];

    let winner = game(player1Move, player2Move, player3Move);
    console.log(`${winner} wins!`);
    })
}

playRockPaperScissorGame();

