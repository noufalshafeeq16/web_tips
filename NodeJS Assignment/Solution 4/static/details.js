var username = "Unknown"
var response = $.getJSON("/config.json", function() {
    var userInfos = response.responseJSON;
    username = userInfos["data"][userInfos["data"].length - 1]["name"];
    var userDetailsTable = document.getElementsByClassName('user_info_table')[0];
    let tableRow = document.createElement("tr");
    for(let field in userInfos["data"][0]){
        let tableHeading = document.createElement("th");
        let heading = document.createTextNode(field);
        tableHeading.appendChild(heading);
        tableRow.appendChild(tableHeading);
    }
    userDetailsTable.appendChild(tableRow);
    for(let userInfo of userInfos["data"]){
        let tableRow = document.createElement("tr");
        for(let field in userInfo){
            let tableData = document.createElement("td");
            let data = document.createTextNode(userInfo[field]);
            tableData.appendChild(data);
            tableRow.appendChild(tableData);
        }
        userDetailsTable.appendChild(tableRow);

    }
  })
    .done(function() {
      console.log( "second success" );
    })
    .fail(function() {
      console.log( "error" );
    })
    .always(function() {
      console.log( "complete" );
    });
var message = document.getElementsByClassName('chat_text_box')[0];
var sendButton = document.getElementsByClassName('chat_send_button')[0];
var chatWindow = document.getElementsByClassName('chat_window')[0];

var socket = io();
sendButton.onclick = ()=>{
  if(message.value != ""){
    socket.emit('chat', `${username} : ${message.value}`);
    message.value="";
  }
};

socket.on('chat', (data)=>{
  chatWindow.innerHTML+=`<p class="chat_message">${data}</p>`;
})

