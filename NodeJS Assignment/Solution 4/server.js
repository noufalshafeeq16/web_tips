const path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var app = express()
var socket = require('socket.io');

app.use(express.static('./static'));

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
})

app.post('/details', urlencodedParser, function (req, res) {
    fs.readFile("./static/config.json", (err, data)=>{
        if(err){
            console.log(err);
        }
        else{
            var userInfos = JSON.parse(data.toString());
            userInfos.data.push(req.body);
            fs.writeFile("./static/config.json",JSON.stringify(userInfos), 'utf8', (err) => {
                if(err){
                    console.log(err);
                }
                else{
                    console.log("New User Information added to file successfully.");
                    res.sendFile(path.join(__dirname+'/details.html'));
                }
            });
        }
    });
})



var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Server listening at http://%s:%s", host, port);
 })

var io = socket(server);

io.on('connection', function(socket){
    console.log('A user is connected.');
    socket.on('chat', (data)=>{
        io.sockets.emit('chat', data);
    })
});

