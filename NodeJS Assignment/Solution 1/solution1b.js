const fs = require("fs");
const inquirer = require('inquirer')

var questions = [{
    type: 'input',
    name: 'name',
    message: "Enter Name : ",
}, {
    type: 'input',
    name: 'gender',
    message: "Enter Gender : ",
}, {
    type: 'input',
    name: 'age',
    message: "Enter Age : ",
}]

fs.readFile("./config.json", (err, data)=>{
    data = JSON.parse(data.toString());
    newData = {};
    inquirer.prompt(questions).then(answers => {
        newData["name"] = answers['name'];
        newData["gender"] = answers['gender'];
        newData["age"] = answers['age'];
        data.data.push(newData);
        fs.writeFile("./config.json",JSON.stringify(data), 'utf8', (err) => {
            if(err){
                console.log(err);
            }
            else{
                console.log("File Updated successfully.");
                console.log("File Contents : \n");
                console.log(data);
            }
        });
    });
});