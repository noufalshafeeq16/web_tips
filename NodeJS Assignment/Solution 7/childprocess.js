const game = require("rockpaperscissor-noufal");

process.on('message', (playerMoves)=>{
    let {player1Move, player2Move, player3Move} = playerMoves;
    let winner = game(player1Move, player2Move, player3Move);
    process.send(winner);
    process.exit(0);
});