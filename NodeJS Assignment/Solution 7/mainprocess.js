const fork = require('child_process').fork;
const inquirer = require('inquirer')


var questions = [{
    type: 'input',
    name: 'player1Move',
    message: "Enter your Move : ",
}]
const gameMoves = ["rock", "paper", "scissor"]

inquirer.prompt(questions).then(answers => {
    let player1Move = answers['player1Move'];
    let player2Move = gameMoves[Math.floor(Math.random() * 3)];
    let player3Move = gameMoves[Math.floor(Math.random() * 3)];
    console.log(`Player : ${player1Move}`);
    console.log(`Computer : ${player2Move}`);
    console.log(`Computer : ${player3Move}`);
    var child = fork('childprocess.js');
    child.send({player1Move:player1Move,
                player2Move:player2Move,
                player3Move:player3Move
    });
    child.on('message', (winner)=>{
        console.log(`${winner} wins!`);
    });
});




