class Line
{
    constructor(p1,p2)
    {
        this.x1=p1[0];
        this.y1=p1[1];
        this.x2=p2[0];
        this.y2=p2[1];
    }
    findDistance(p1, p2)
    {
        var x1, x2, y1, y2;

        if (p1 && p2) {
        x1 = p1[0];
        x2 = p2[0];
        y1 = p1[1];
        y2 = p2[1];
        } else {
        x1 = this.x1;
        x2 = this.x2;
        y1 = this.y1;
        y2 = this.y2;
        }

        return Math.sqrt(Math.pow(x2-x1, 2)+ Math.pow(y2-y1, 2));
    }

    findQuadrant()
    {
        var x=(parseFloat(this.x1)+parseFloat(this.x2));
        var y=(parseFloat(this.y1)+parseFloat(this.y2));
        if(x>=0&&y>=0)
            return 1;
        else if(x<0&&y>=0)
            return 2;
        else if(x<0&&y<0)
            return 3;
        else
            return 4;
    }
}

class Triangle extends Line
{
    constructor(p1,p2,p3)
    {
        super(p1,p2);
        this.x1=p1[0];
        this.y1=p1[1];
        this.x2=p2[0];
        this.y2=p2[1];
        this.x3=p3[0];
        this.y3=p3[1];
    }

    findType()
    {
        var d1=this.findDistance([this.x1,this.y1],[this.x2, this.y2]);
        var d2=this.findDistance([this.x2,this.y2],[this.x3, this.y3]);
        var d3=this.findDistance([this.x3,this.y3],[this.x1, this.y1]);
        if(d1 == d2 && d2 == d3 && d3 == d1)
            return "Equilateral";
        else if(d1==d2||d2==d3||d1==d3)
            return "Isosceles";
        else
            return "Scalene";
    }

    findArea(p1,p2,p3)
    {
        var x1,x2,x3,y1,y2,y3,area;
        if(p1&&p2&&p3)  //if parameters are present, compute area of parameters.
        {
            x1=p1[0];
            x2=p2[0];
            x3=p3[0];
            y1=p1[1];
            y2=p2[1];
            y3=p3[1];
        }
        else            //compute distance of its own points (properties).
        {
            x1=this.x1;
            x2=this.x2;
            x3=this.x3;
            y1=this.y1;
            y2=this.y2;
            y3=this.y3;
        }
        area = ((x1*y2+x2*y3+x3*y1)-
            (x1*y3+x2*y1+x3*y2))/2;
        return Math.abs(area);
    }
}

class Quadilateral extends Line
{
    constructor(p1,p2,p3,p4)
    {
        super(p1,p2);
        this.x1=p1[0];
        this.y1=p1[1];
        this.x2=p2[0];
        this.y2=p2[1];
        this.x3=p3[0];
        this.y3=p3[1];
        this.x4=p4[0];
        this.y4=p4[1];
    }

    findType()
    {
        /*----------COMPUTE TOTAL NUMBER OF EQUAL SIDES------------*/

        var numberOfEqualSides=0;             //Total number of equal sides.
        var d1=this.findDistance([this.x1, this.y1],[this.x2, this.y2]);
        var d2=this.findDistance([this.x2, this.y2],[this.x3, this.y3]);
        var d3=this.findDistance([this.x3, this.y3],[this.x4, this.y4]);
        var d4=this.findDistance([this.x4, this.y4],[this.x1, this.y1]);
        if((d1==d2)&&(d2==d3)&&(d3==d4))
            numberOfEqualSides=4;
        else if (d1==d3&&d2==d4)
            numberOfEqualSides=2;
        else if(d1==d3||d2==d4)
            numberOfEqualSides=1;

        /*----------FIND IF DIAGONALS ARE EQUAL OR NOT--------------*/

        var diag1=this.findDistance([this.x1, this.y1],[this.x3, this.y3]);
        var diag2=this.findDistance([this.x2, this.y2],[this.x4, this.y4]);
        var isDiagonalEqual = diag1==diag2;  //if both the diagonal lengths are equal or not.

        /*--------COMPUTE TOTAL NUMBER OF PARALLEL SIDES------------*/

        var m1=slope([this.x1, this.y1],[this.x2, this.y2]);
        var m2=slope([this.x2, this.y2],[this.x3, this.y3]);
        var m3=slope([this.x3, this.y3],[this.x4, this.y4]);
        var m4=slope([this.x4, this.y4],[this.x1, this.y1]);
        if(m1==-Infinity)
            m1=Infinity;
        if(m2==-Infinity)
            m2=Infinity;
        if(m3==-Infinity)
            m3=Infinity;
        if(m4==-Infinity)
            m4=Infinity;
        var numberOfParallelSides=0;  //no. of parallel sides
        if(m1==m3)
            numberOfParallelSides+=2;
        if(m2==m4)
            numberOfParallelSides+=2;

        if(numberOfParallelSides==0)
        {
            return("Other Quadilateral");
        }
        if(numberOfParallelSides==2)
        {
            return("Trapezium");
        }
        if(numberOfEqualSides==4)
        {
            if(isDiagonalEqual)
            {
                return("Square");
            }
            else
            {
                return("Rhombus");
            }
        }
        else if(numberOfEqualSides==2)
        {
            if(isDiagonalEqual)
            {
                return("Rectangle");
            }
            else
            {
                return("Parallelogram");
            }
        }
    }

    findArea()
    {
        var abc=new Triangle([this.x1, this.y1],[this.x2, this.y2],[this.x3, this.y3]).findArea();
        var acd=new Triangle([this.x1, this.y1],[this.x3, this.y3],[this.x4, this.y4]).findArea();
        var area=abc+acd;
        return area;
    }
}


/*---------------------------------------------------------
    TO CONVERT INPUT STRING FROM FILE TO ARRAY OF POINTS
---------------------------------------------------------*/
function stringToArray(arr)
{
for(var i=0;i<arr.length;i++)
    {
        arr[i]=arr[i].replace(/ /g,'');
        arr[i]=arr[i].substring(1,arr[i].length-2);
        arr[i]=arr[i].split("],[");
        arr[i][0]=arr[i][0].replace("[","");
        arr[i][arr[i].length-1]=arr[i][arr[i].length-1].replace("]","");
        for(var j=0;j<arr[i].length;j++)
        {
            arr[i][j] = arr[i][j].split(",");
        }
    }
    return arr;
}


function slope(a,b)
{
    return (b[1]-a[1])/(b[0]-a[0]);
}
/*---------------------------------------------------------------
                MAIN PROGRAM STARTS HERE
----------------------------------------------------------------*/

var fs=document.getElementById("files");
var data;
fs.onchange=function(){
    const reader=new FileReader();
    reader.readAsText(fs.files[0]);
    reader.onload=function()
    {
        data=reader.result;
        if(!data)    //throw errors if data has empty String.
            throw new Error("File is Empty");
        var arr=data.split("\n");
        arr=stringToArray(arr);
        for(var i=0;i<arr.length;i++)
        {
            if(arr[i].length==2)
            {
                console.log("======LINE======");
                let line=new Line(arr[i][0],arr[i][1]);
                console.log("Quadrant:",Line.prototype.findQuadrant.apply(line, arr[i]));
                console.log("Distance:",Line.prototype.findDistance.call(line));
                console.log();
            }
            else if(arr[i].length==3)
            {
                console.log("======TRIANGLE======");
                let triangle=new Triangle(arr[i][0],arr[i][1], arr[i][2]);
                console.log("Type:", Triangle.prototype.findType.call(triangle));
                console.log("Area:", Triangle.prototype.findArea.apply(triangle));
                console.log();
            }
            else if(arr[i].length==4)
            {
                console.log("=======QUADILATERAL======");
                let quad=new Quadilateral(arr[i][0],arr[i][1], arr[i][2],arr[i][3]);
                console.log("Type:",Quadilateral.prototype.findType.call(quad));
                console.log("Area:",Quadilateral.prototype.findArea.apply(quad));
                console.log();
            }
            else
            {
                //throw errors if data has no points or less points or more points than expected.
                throw new Error("Invalid number of points. Number of points should be 2, 3 or 4.");
            }
        }
    }
}
