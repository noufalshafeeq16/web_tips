function mainClosure()
{
    var privateCounter=0;
    return function incrementCounter(incrementer)
    {
        return privateCounter+=incrementer;
    };
}

var mainIncrementer = mainClosure();

console.log(mainIncrementer(1));
console.log(mainIncrementer(1));
console.log(mainIncrementer(3));
console.log(mainIncrementer(5));
console.log(mainIncrementer(1));