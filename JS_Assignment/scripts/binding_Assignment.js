function divide(num,den)
{
    return num/den;
}

function curry(den){
    return (num) => {
        return divide(num, den);
    }
}

let divideBy2 = curry(2);
let divideBy5 = curry(5);

console.log(divideBy2(10));
console.log(divideBy5(100));

